#include <iostream>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <vector>
#include <boost/assign.hpp>

using namespace std;
using namespace boost::assign;

int f2(int x, int y)
{
    cout << "f2(" << x << ", " << y << ")\n";
    return x + y;
}

int f3(int x, int y, int z)
{
    cout << "f3(" << x << ", " << y << ", " << z << ")\n";
    return x + y + z;
}

void modify_arg(const std::string& desc, int& x, int& y)
{
    cout << desc << endl;
    x = 1;
    y = 0;
}

class Functor : public std::binary_function<int, int, int>
{
public:
    int operator()(int x, int y)
    {
        cout << "Functor(" << x << ", " << y << ")" << endl;
        return x + y;
    }
};

class Person
{
    string name_;
public:
    Person(const string& name) : name_(name)
    {
    }

    void print(const string& prefix) const
    {
        cout << prefix << name_ << endl;
    }
};

int main()
{
    auto boundf2 = boost::bind(&f2, 1, _1);

    boundf2(2); // -> f2(1, 2)

    auto boundf3 = boost::bind(&f3, _2, 7, _1);

    boundf3(1, 3); //  -> f3(1, 7, 3)

    auto boundf3_0 = boost::bind(&f3, 1, 2, 3);

    boundf3_0();

    int x = 10;
    int y = 20;
    string str = "Test modify_arg";

    auto f = boost::bind(&modify_arg, boost::cref(str), boost::ref(x), _1);

    f(y);

    cout << "x = " << x << "; y = " << y << endl;

    Functor func;

    auto bound_func = boost::bind(func, _1, 2);

    int result = bound_func(10);

    boost::shared_ptr<Person> p1 = boost::make_shared<Person>("Kowalski");

    auto bound_print = boost::bind(&Person::print, p1, "Person: ");

    bound_print();

    vector<Person> vec = list_of(Person("Kowlaski"))(Person("Nowak"))(Person("Nijaki"));

    cout << "\nList of persons:\n";
    for_each(vec.begin(), vec.end(), boost::bind(&Person::print, _1, "Osoba: "));

    vector<Person*> vecptr = list_of(new Person("Kowlaski"))(new Person("Nowak"))(new Person("Nijaki"));

    cout << "\nList of persons:\n";
    for_each(vecptr.begin(), vecptr.end(), boost::bind(&Person::print, _1, "Osoba: "));

    for_each(vecptr.begin(), vecptr.end(), &boost::checked_delete<Person>);

    vector<boost::shared_ptr<Person> > vecsptr = list_of(boost::make_shared<Person>("Kowlaski"))
            (boost::make_shared<Person>("Nowak"))(boost::make_shared<Person>("Nijaki"));

    cout << "\nList of persons:\n";
    for_each(vecsptr.begin(), vecsptr.end(), boost::bind(&Person::print, _1, "Osoba: "));
}

