#include "person.hpp"
#include "utils.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>

using namespace std;

int main()
{
	vector<Person> employees;
	fill_person_container(employees);

	cout << "Wszyscy pracownicy:\n";
	copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
	cout << endl;

	// wyswietl pracownikow z pensja powyzej 3000
	cout << "\nPracownicy z pensja powy�ej 3000:\n";

	// wy�wietl pracownik�w o wieku poni�ej 30 lat
	cout << "\nPracownicy o wieku poni�ej 30 lat:\n";
	
	// posortuj malej�co pracownikow wg nazwiska
	cout << "\nLista pracownik�w wg nazwiska (malejaco):\n";
	
	// wy�wietl kobiety
	cout << "\nKobiety:\n";
	
	// ilosc osob zarabiajacych powyzej sredniej
	cout << "\nIlosc osob zarabiajacych powyzej sredniej:\n";
}