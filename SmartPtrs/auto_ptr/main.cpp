#include <memory>
#include <iostream>
#include <exception>
#include <stdexcept>
#include <cassert>
#include <vector>

using namespace std;

class X
{
public:
	// konstruktor
	X(int value = 0)
		: value_(value)
	{
		std::cout << "Konstruktor X(" << value_ << ")\n"; 
	}
	
	// destruktor
	~X() 
	{
		std::cout << "Destruktor ~X(" << value_ << ")\n"; 
	}
	
	int& value()
	{
		return value_;
	}

	const int& value() const
	{
		return value_;
	}

    void unsafe()
    {
        throw std::runtime_error("ERROR");
    }

private:
	int value_;
};

std::auto_ptr<X> factory(int arg) // TODO: poprawa z wykorzystaniem auto_ptr
{
    return std::auto_ptr<X>(new X(arg));
}

void legacy_cleanup(X* ptr)
{
    delete ptr;
}

void unsafe1()  // TODO: poprawa z wykorzystaniem auto_ptr
{
    std::auto_ptr<X> ptrX(factory(4)); // konstruktor kopiujacy

	/* kod korzystajacy z ptrX */
    ptrX->unsafe();

    std::auto_ptr<X> copyOfPtrX = ptrX; // kopiowanie auto_ptr<X>

    assert(ptrX.get() == NULL);
    assert(copyOfPtrX.get() != NULL);

    copyOfPtrX.reset(new X(10));

    legacy_cleanup(copyOfPtrX.release());
    assert(copyOfPtrX.get() == NULL);
}

namespace CPP11
{
    std::unique_ptr<X> factory(int arg) // TODO: poprawa z wykorzystaniem auto_ptr
    {
        return std::unique_ptr<X>(new X(arg));
    }

    void unsafe1()  // TODO: poprawa z wykorzystaniem auto_ptr
    {
        std::unique_ptr<X> createdX(factory(4));
        std::unique_ptr<X> ptrX(std::move(createdX)); // konstruktor kopiujacy

        /* kod korzystajacy z ptrX */
        //ptrX->unsafe();

        std::unique_ptr<X> copyOfPtrX = std::move(ptrX); // kopiowanie auto_ptr<X>

        assert(ptrX.get() == NULL);
        assert(copyOfPtrX.get() != NULL);

        copyOfPtrX.reset(new X(10));

        legacy_cleanup(copyOfPtrX.release());
        assert(copyOfPtrX.get() == NULL);

        vector<unique_ptr<X>> vecX;

        unique_ptr<X> object(new X(12));

        vecX.push_back(unique_ptr<X>(new X(10)));
        vecX.push_back(factory(11));
        vecX.push_back(std::move(object));

        cout << vecX[0]->value() << endl;
    }
}

int main()
{
	try 
	{
        //unsafe1();
        CPP11::unsafe1();
	}
	catch(...)
	{
		std::cout << "Zlapalem wyjatek!" << std::endl;
	}
}
