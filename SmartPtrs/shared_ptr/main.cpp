#include <iostream>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <cassert>
#include <cstdlib>
#include <stdexcept>

class X
{
public:
	// konstruktor
	X(int value = 0)
		: value_(value)
	{
		std::cout << "Konstruktor X()\n"; 
	}
	
	// destruktor
	~X() 
	{
		std::cout << "Destruktor ~X()\n"; 
	}
	
	int& value()
	{
		return value_;
	}

	const int& value() const
	{
		return value_;
	}
private:
	int value_;
};

class A
{
	boost::shared_ptr<X> x_;
public:
	A(boost::shared_ptr<X> x) : x_(x) {}
	
	int get_value()
	{
		return x_->value();
	}
};

class B
{
	boost::shared_ptr<X> x_;
public:
	B(boost::shared_ptr<X> x) : x_(x) {}
	
	void set_value(int i)
	{
		x_->value() = i;
	}	
};

class Base
{
protected:
    int id_;
public:
    Base(int id) : id_(id)
    {}

    virtual void do_sth()
    {
        std::cout << "Base::do_sth(" << id_ << ")\n";
    }

    virtual ~Base() {}
};

class Derived : public Base
{
public:
    Derived(int id) : Base(id)
    {
    }

    virtual void do_sth()
    {
        std::cout << "Derived::do_sth(" << id_ << ")\n";
    }

    void only_in_derived()
    {
        std::cout << "Derived::only_in_derived()\n";
    }
};

void foo(boost::shared_ptr<X> ptrX, int value)
{

}

int gen_value()
{
    throw std::runtime_exception("ERROR");
}

int main()
{
    boost::shared_ptr<X> ptrX(new X());
    foo(ptrX, gen_value());

	{
        boost::shared_ptr<X> spX1 = boost::make_shared<X>();
		std::cout << "RC: " << spX1.use_count() << std::endl;
		{
			boost::shared_ptr<X> spX2(spX1);
			std::cout << "RC: " << spX2.use_count() << std::endl;
		}
		std::cout << "RC: " << spX1.use_count() << std::endl;
	}

	std::cout << "\n//----------------------\n\n";

    {
        boost::shared_ptr<X> temp( new X(14) );
        A a(temp);
        {
            B b(temp);
            b.set_value(28);
        }

        assert(a.get_value() == 28);
        std::cout << "a.get_value() = " << a.get_value() << std::endl;
    }
    std::cout << "\n//----------------------\n\n";

    boost::shared_ptr<Base> basePtr(new Base(1));

    basePtr->do_sth();

    boost::shared_ptr<Derived> derivedPtr
            = boost::dynamic_pointer_cast<Derived>(basePtr);

    if (derivedPtr)
        derivedPtr->only_in_derived();
    else
        std::cout << "Nieprawidlowe rzutowanie\n";
}






