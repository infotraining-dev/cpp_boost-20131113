#include <boost/shared_ptr.hpp>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <boost/make_shared.hpp>
#include <algorithm>

using namespace std;

class Object
{
public:
	Object(int val = 0) : val_(val) 
	{
		cout << "Konstrukcja obiektu Object " << val_ << "\n";
	}
	
	~Object()
	{
		cout << "Destrukcja obiektu Object " << val_ << "\n";
	}
	
	void print() const
	{
		cout << "Działa obiekt klasy Object " << val_ << "\n";
	}
private:
	int val_;
};

int main()
{
	boost::shared_ptr<Object> external;
	try
	{
        std::vector<boost::shared_ptr<Object> > vec;
		
        std::auto_ptr<Object> aptr(new Object(5));

		vec.push_back(boost::shared_ptr<Object>(new Object(1)));
        vec.push_back(boost::make_shared<Object>(2));
		vec.push_back(boost::shared_ptr<Object>(new Object(3)));
		vec.push_back(boost::shared_ptr<Object>(new Object(4)));
        vec.push_back(boost::shared_ptr<Object>(aptr));
		
		vec[0]->print();

        external = vec[0]; // kopiowanie elementu wektora
		
		throw int(5);
	}
	catch(...)
	{
		cout << "Zlapalem wyjątek\n";
	}
}
