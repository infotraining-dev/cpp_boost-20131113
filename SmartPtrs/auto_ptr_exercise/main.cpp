#include <iostream>
#include <cstdlib>
#include <exception>
#include <stdexcept>
#include <memory>
#include <boost/scoped_ptr.hpp>

using namespace std;

/********************************************************************
*  Uodparnianie konstrukora na wyjątki #1
********************************************************************/

class Device
{
private:
	size_t devno_;
public:
	Device(int devno) : devno_(devno)
	{
		if (devno == 2)
			throw std::runtime_error("Powazny problem!");

		cout << "Konstruktor Device #" << devno << endl;
	}

    int devno() const
    {
        return devno_;
    }

	~Device()
	{
		cout << "Destruktor Device #" << devno_ << endl;
	}

};

class Broker 
{
public:
    Broker(int devno1, int devno2) : dev1_(new Device(devno1)), dev2_(new Device(devno2))
	{
	}

    Broker(const Broker& source) : dev1_(new Device(source.dev1_->devno())), dev2_(new Device(source.dev2_->devno()))
    {
    }

	~Broker()
	{
	}
private:
    boost::scoped_ptr<Device> dev1_;
    boost::scoped_ptr<Device> dev2_;
};

// idiom PIMPL

// plik Utility.hpp
class Utility
{
    class UtilityImpl;

    boost::scoped_ptr<UtilityImpl> impl_;
public:
    void use();
};

// plik UtilityImpl.hpp

class UtilityImpl
{
public:
    void create_buffer();
    void release_buffer();
};



namespace CPP11
{
    class Broker
    {
    public:
        Broker(int devno1, int devno2) : dev1_(new Device(devno1)), dev2_(new Device(devno2))
        {
        }

        Broker(Broker&& source) : dev1_(std::move(source.dev1_)), dev2_(std::move(source.dev2_))
        {
        }

        ~Broker()
        {
        }
    private:
        std::unique_ptr<Device> dev1_;
        std::unique_ptr<Device> dev2_;
    };
}

int main()
{
	try
	{
        Broker b(1, 3);

        Broker copyOfB = b;

        // Broker ownerOfB = std::move(b); // jawny transfer wlasnosci do unique_ptr

        // b.dev1_ = NULL
        // b.dev2_ = NULL

        // ownerOfB.dev1_ = 0xFFAAFF;
        // ownerOfB.dev2_ = 0xFFBBFF;
	}
	catch(const exception& e)
	{
		cerr << "Wyjatek: " << e.what() << endl;
	}
}
