#include <iostream>
#include <vector>

using namespace std;

class Mutex
{
public:
    void lock()
    {
        cout << "Mutex is locked." << endl;
    }

    void unlock()
    {
        cout << "Mutex is unlocked" << endl;
    }
};

class LockGuard
{
    Mutex& mutex_;
    LockGuard(const LockGuard&);
    LockGuard& operator=(const LockGuard&);
public:
    LockGuard(Mutex& mutex) : mutex_(mutex)
    {
        mutex_.lock();
    }

    ~LockGuard()
    {
        mutex_.unlock();
    }
};

class Object
{
    Mutex mutex_;
    vector<int> buffer_;
public:
    void do_sth()
    {
        LockGuard lock(mutex_);

        buffer_.at(12) = 10;
    }
};

int main()
{
    try
    {
        Object o;

        o.do_sth();
    }
    catch(const std::exception& e)
    {
        cout << "Obsluga wyjatku: " << e.what() << endl;
    }

    return 0;
}

