#include <iostream>
#include <vector>
#include <list>
#include <boost/assign.hpp>
#include <boost/foreach.hpp>

using namespace std;

int main()
{
    using namespace boost::assign;

    vector<int> vec = list_of(-1)(0);

    vec.push_back(1);
    vec.push_back(2);
    vec.push_back(3);
    vec.push_back(4);

    vec += 5, 6, 7, 8, 9, 10;

    BOOST_FOREACH(int item, vec)
    {
        cout << "item: " << item << endl;
    }
}

