#include <iostream>
#include <boost/utility.hpp>

//class PleaseDontMakeCopies
//{
//public:
//	PleaseDontMakeCopies() {};
//private:
//	PleaseDontMakeCopies(const PleaseDontMakeCopies&);
//	PleaseDontMakeCopies& operator=(const PleaseDontMakeCopies&);
//};

class PleaseDontMakeCopies : boost::noncopyable
{
public:
	void doStuff()
	{
		std::cout << "Informuje się użytkowników o niemożliwości kopiowania" << std::endl;
	}
};

int main()
{
	PleaseDontMakeCopies a;
	
    //PleaseDontMakeCopies b = a;  // konstruktor kopiujący
    //PleaseDontMakeCopies c(a);   // konstruktor kopiujący

    c = b; // operator przypisania
}
