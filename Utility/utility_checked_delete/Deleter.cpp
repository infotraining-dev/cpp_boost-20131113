#include <iostream>
#include "Deleter.hpp"
#include "ToBeDeleted.hpp"
#include <boost/checked_delete.hpp>

void Deleter::delete_it(ToBeDeleted* p)
{
    std::cout << "delete_it called" << std::endl;

    //delete p;
    boost::checked_delete(p);
}
