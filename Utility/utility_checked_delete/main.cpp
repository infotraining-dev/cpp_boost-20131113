#include "Deleter.hpp"
#include "ToBeDeleted.hpp"
#include <vector>
#include <algorithm>
#include <boost/checked_delete.hpp>

void real_test()
{
	ToBeDeleted* tbd = new ToBeDeleted();

	Deleter exterminator;
	exterminator.delete_it(tbd);
}

int main()
{
    {
        std::vector<int*> vecptrint;

        vecptrint.push_back(new int(4));
        vecptrint.push_back(new int(5));

        std::for_each(vecptrint.begin(), vecptrint.end(), &boost::checked_delete<int>);
    }


	real_test();
}
