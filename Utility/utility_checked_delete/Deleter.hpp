#ifndef DELETER_HPP_
#define DELETER_HPP_

class ToBeDeleted; // deklaracja zapowiadajaca

class Deleter
{
public:
	void delete_it(ToBeDeleted* p);
};

#endif /* DELETER_HPP_ */
