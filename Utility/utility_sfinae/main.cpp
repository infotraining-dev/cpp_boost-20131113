#include <iostream>
#include <string>

using namespace std;

struct WithNestedType
{
    typedef string type;
};

void some_func(int i)
{
   cout << "void some_func(int i)\n";
}

template <typename T> typename T::type* some_func(T t)
{
   typename T::type variable_of_nested_type;

   cout << "template <typename> void some_func(T t) with nested type\n";
}

int main()
{
    int i = 13;
    short s = 13;
    some_func(i);  // wywołana void some_func(int i)
    some_func(s);  // błąd kompilacji! short in is not a class

    WithNestedType wnt;
    some_func(wnt);
}
