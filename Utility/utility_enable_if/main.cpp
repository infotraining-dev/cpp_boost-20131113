#include <iostream>
#include <iterator>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits.hpp>
#include <vector>
#include <list>

class ClassWithType
{
public:
    typedef int type;
};

class ExtraClass
{
};

// type trait dla typu ExtraClass
template <typename T>
struct is_extra : public boost::false_type {};

template <>
struct is_extra<ExtraClass> : public  boost::true_type {};


void some_func(int i)
{
    std::cout << "void some_func(" << i << ")\n";
}

template <typename T>
void some_func(T t, typename boost::enable_if<boost::is_unsigned<T> >::type* = 0)
{
	std::cout << "template <typename T> void some_func(T t) : ograniczenie T jest unsigned\n";
}

template <typename T>
void some_func(T t, typename boost::enable_if<is_extra<T> >::type* = 0)
{
	std::cout << "template <typename T> void some_func(T t) : ograniczenie T jest typu ExtraClass\n";
}

template <typename T>
void some_func(T t, typename boost::disable_if<boost::is_integral<T> >::type* = 0,
        typename boost::disable_if<is_extra<T> >::type* = 0)
{
    typename T::type variable_of_nested_type;
    std::cout << "template <typename T> void some_func(T t)\n";
}

template <typename InIt, typename OutIt>
void mcopy(InIt start, InIt end, OutIt dest)
{
    std::cout << "Generic copy" << std::endl;
}

template <typename T>
void mcopy(T* start, T* end, T* dest, typename  boost::enable_if<boost::is_pod<T> >::type* = 0)
{
    std::cout << "Memcopy" << std::endl;
}


namespace EnableIfWithClassTemplates
{
    template <class T, class Enable = void>
    class data_processor
    {
    public:
        void process_data()
        {
            std::cout << "Generic processor." << std::endl;
        }
    };

    template <class T>
    class data_processor<T, typename boost::enable_if_c<boost::is_integral<T>::value>::type>
    {
    public:
        void process_data()
        {
            std::cout << "Integral processor." << std::endl;
        }
    };

    // SSE optimized version for float types
    template <class T>
    class data_processor<T, typename boost::enable_if_c<boost::is_float<T>::value>::type>
    {
    public:
        void process_data()
        {
            std::cout << "Float processor." << std::endl;
        }
    };

}

int main()
{
	int i = 12;
	short s = 12;
	unsigned int ui = 12;
	unsigned short us = 12;
	ClassWithType cwt;
	ExtraClass ec;

	some_func(i);
	some_func(s);
    some_func(ui);
    some_func(us);
	some_func(cwt);
	some_func(ec);

    std::cout << "\n---------------\n\n";

    std::vector<int> v1(5);
    std::vector<int> v2(5);
    std::list<int> l1(5);
    int tab1[5];
    int tab2[5];
    std::string str1[5];
    std::string str2[5];

    mcopy(v1.begin(), v1.end(), v2.begin());
    mcopy(v1.begin(), v1.end(), l1.begin());
    mcopy(&tab1, &tab1+5, &tab2);
    mcopy(&str1, &str1+5, &str2);

    std::cout << "\n---------------\n\n";

    EnableIfWithClassTemplates::data_processor<std::string> dproc_string;
    dproc_string.process_data();

    EnableIfWithClassTemplates::data_processor<int> dproc_int;
        dproc_int.process_data();

     EnableIfWithClassTemplates::data_processor<float> dproc_float;
     dproc_float.process_data();
}
