#include <iostream>
#include <string>
#include <boost/cast.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

int main() try
{
    int x = 270;
    char cx;

    //cx = boost::numeric_cast<char>(x);

    x = -270;

    unsigned int ux = 128;

    //ux = boost::numeric_cast<unsigned int>(x);

    string str1 = "ux = " + boost::lexical_cast<string>(ux);

    cout << str1 << endl;

    string strPi = "3.1415";

    double pi = boost::lexical_cast<double>(strPi);

    cout << "pi = " << pi << endl;

    return 0;
}
catch(const std::exception& e)
{
    cout << "Złapano wyjątek: " << e.what() << endl;
}

