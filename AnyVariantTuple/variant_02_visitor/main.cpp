#include <iostream>
#include <vector>
#include <complex>
#include <algorithm>
#include <list>
#include <boost/variant.hpp>

using namespace std;

void times_two(boost::variant<int, string, complex<double> >& v)
{
	if (int* pi = boost::get<int>(&v))
		*pi *= 2;
	else if (string* pstr = boost::get<string>(&v))
		*pstr += *pstr;
}

class TimesTwoVisitor :public boost::static_visitor<>
{
public:
	void operator()(int& i) const
	{
		i *= 2;
	}

	void operator()(std::string& str) const
    {
		str += str;
	}

    void operator()(std::complex<double>& c) const
    {
        c += c;
    }
};

template <typename Type>
class ExtractorVisitor : public boost::static_visitor<>
{
    std::list<Type>& container_;
public:
    ExtractorVisitor(std::list<Type>& container) : container_(container)
    {
    }

    template <typename T>
    void operator()(const T& n) const
    {
    }

    void operator()(Type& item) const
    {
        container_.push_back(item);
    }
};

int main()
{
    boost::variant<int, string, complex<double> > var;

	var = 5;
	times_two(var);
	cout << "var = " << var << endl;

	var = "Tekst...";
	times_two(var);
	cout << "var = " << var << endl;

    var = complex<double>(2.0, 4.0);
    times_two(var);
    cout << "var = " << var << endl;

    cout << "\n\n-------------------------\n";

    // to samo z wizytorem
	boost::apply_visitor(TimesTwoVisitor(), var);
	cout << "var = " << var << endl;

    // wizytacja opozniona
	vector<boost::variant<int, string> > vars;
	vars.push_back(1);
	vars.push_back("two");
	vars.push_back(3);
	vars.push_back("four");

	TimesTwoVisitor visitor;
	for_each(vars.begin(), vars.end(), boost::apply_visitor(visitor));

    cout << "vars: ";
	copy(vars.begin(), vars.end(), ostream_iterator<boost::variant<int, string> >(cout, " "));
	cout << endl;

    list<int> ints;
    list<string> strings;

    ExtractorVisitor<int> extractor_ints(ints);
    for_each(vars.begin(), vars.end(), boost::apply_visitor(extractor_ints));

    ExtractorVisitor<string> extractor_strings(strings);
    for_each(vars.begin(), vars.end(), boost::apply_visitor(extractor_strings));

    cout << "ints: ";
    copy(ints.begin(), ints.end(), ostream_iterator<int>(cout,  " "));

    cout << "\nstrings: ";
    copy(strings.begin(), strings.end(), ostream_iterator<string>(cout,  " "));
}
