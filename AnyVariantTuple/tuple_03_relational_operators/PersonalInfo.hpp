#include <string>
#include <iostream>

class PersonalInfo
{
	std::string first_name_;
	std::string last_name_;
	unsigned int age_;
public:
	PersonalInfo(const std::string& fn, const std::string& ln, unsigned int age) :
		first_name_(fn), last_name_(ln), age_(age)
	{
	}

	std::string first_name() const
	{
		return first_name_;
	}

	std::string last_name() const
	{
		return last_name_;
	}

	unsigned int age() const
	{
		return age_;
	}
};

std::ostream& operator<<(std::ostream& out, const PersonalInfo& pi)
{
	out << "PersonalInfo(" << pi.first_name() << ", " << pi.last_name()<< ", " << pi.age() << ")";

	return out;
}

bool operator==(const PersonalInfo& pi1, const PersonalInfo& pi2)
{
	return (pi1.first_name() == pi2.first_name())
			&& (pi1.last_name() == pi2.last_name())
			&& (pi1.age() == pi2.age());
}

bool operator<(const PersonalInfo& pi1, const PersonalInfo& pi2)
{
	return pi1.last_name() < pi2.last_name();
}

bool operator>(const PersonalInfo& pi1, const PersonalInfo& pi2)
{
    return pi1.last_name() > pi2.last_name();
}
