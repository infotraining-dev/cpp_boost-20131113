#include <iostream>
#include <string>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_io.hpp>

using namespace std;

boost::tuple<int, char, char> foo(const string& str)
{
    //return boost::tuple<int, char, char>(str.length(), str[0], str[str.length()-1]);

    return boost::make_tuple(str.length(), str[0], str[str.length()-1]);
}

int main()
{
    using namespace boost;

    tuple<int, char, char> result = foo("Tekst");

    cout << "length = " << result.get<0>()
         << " first char = " <<  get<1>(result)
         << " last char = " << get<2>(result) << endl;

    int length;
    char first, last;

    tie(length, first, last) = foo("NSN");

    cout << "length = " << length
         << " first char = " <<  first
         << " last char = " << last << endl;

    tie(tuples::ignore, first, last) = foo("Wroclaw");

    cout << "first char = " <<  first
         << " last char = " << last << endl;

    cout << "krotka: " << tuples::set_open('{')
         << tuples::set_close('}')
         << tuples::set_delimiter(',')
         << foo("Test") << endl;

    cout << foo("Inna krotka") << endl;

    return 0;
}

