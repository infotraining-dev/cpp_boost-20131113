#include <iostream>
#include <boost/any.hpp>
#include <string>

using namespace std;

class X
{
    int value_;
public:
    X(int value) : value_(value)
    {
    }

    int value() const
    {
        return value_;
    }
};

int main() try
{
    boost::any a1;

    if (a1.empty())
        cout << "a1 jest puste" << endl;

    a1 = 5;

    a1 = string("Adam");

    a1 = 3.14;

    a1 = X(10);

    X x = boost::any_cast<X>(a1);

    X* ptrx = boost::any_cast<X>(&a1);

    cout << "x.value() = " << x.value() << endl;

    cout << "ptrx->value() = " << ptrx->value() << endl;

    cout << a1.type().name() << endl;

    if (a1.type() == typeid(X))
    {
        cout << "w any jest X" << endl;
    }

    string* ptrStr = boost::any_cast<string>(&a1);

    if (!ptrStr)
        cout << "ptrStr jest NULL" << endl;

    int n = boost::any_cast<int>(a1);

    ptrx = boost::unsafe_any_cast<X>(&a1);

    return 0;
}
catch(const exception& e)
{
    cout << e.what() << endl;
}

