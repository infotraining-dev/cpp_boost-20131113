#include <iostream>
#include <vector>
#include <list>
#include <iterator>
#include <functional>
#include <map>
#include <stdexcept>
#include <boost/any.hpp>

using namespace std;

template <typename T>
class IsType : public unary_function<boost::any, bool>
{
public:
    bool operator()(const boost::any& a) const
    {
        return a.type() == typeid(T);
    }
};

class key_not_found : public logic_error
{
public:
    key_not_found(const string& message) : logic_error(message)
    {
    }
};

class invalid_value_type : public logic_error
{
public:
    invalid_value_type(const string& message) : logic_error(message)
    {
    }
};

class MapAny
{
    typedef map<string, boost::any> MapType;
    MapType map_;
public:
    template <typename ValueType>

    void insert(const string& key, const ValueType& value)
    {
        map_.insert(make_pair(key, boost::any(value)));
    }

    template <typename ValueType>
    ValueType get(const string& key) const
    {
        MapType::iterator it = map_.find(key);

        if (it == map_.end())
            throw key_not_found("Key not found:  " + key);

        ValueType* ptr_value = NULL;

        ptr_value = boost::any_cast<ValueType>(&(it->second));

        if (ptr_value == NULL)
        {
            throw invalid_value_type("Invalid value type");
        }

        return *ptr_value;
    }
};

int main()
{
	vector<boost::any> store_anything;

	store_anything.push_back(1);
	store_anything.push_back(5);
	store_anything.push_back(string("three"));
	store_anything.push_back(3);
	store_anything.push_back(string("four"));
	store_anything.push_back(string("one"));
	store_anything.push_back(string("eight"));
	store_anything.push_back(5);
	store_anything.push_back(4);
	store_anything.push_back(boost::any());
	store_anything.push_back(string("five"));
	store_anything.push_back(string("six"));
	store_anything.push_back(boost::any());

	/* TO DO :
     * Wykorzystując algorytmy biblioteki standardowej wykonaj nastapujące czynnosci (napisz odpowiednie
	 * do tego celu predykaty lub obiekty funkcyjne):
     * 1 - przefiltruj wartosci niepuste w kolekcji stored_anything
	 * 2 - zlicz ilosc elementow typu int oraz typu string
	 * 3 - wyekstraktuj z kontenera store_anything do innego kontenera wszystkie elementy typu string
	 */

	// 1
	vector<boost::any> non_empty;
	// TODO

    remove_copy_if(store_anything.begin(), store_anything.end(),
                   back_inserter(non_empty), mem_fn(&boost::any::empty));

	cout << "store_anything.size() = " << store_anything.size() << endl;
	cout << "non_empty.size() = " << non_empty.size() << endl;

	// 2
	int count_int;
	// TODO
    count_int = count_int = count_if(store_anything.begin(), store_anything.end(), IsType<int>());

    cout << "stored_anything przechowuje " << count_int << " elementow typu int" << endl;

    int count_string;
    // TODO

    count_string = count_int = count_if(store_anything.begin(), store_anything.end(), IsType<string>());

    cout << "stored_anything przechowuje " << count_string << " elementow typu string" << endl;

	// 3
	list<string> string_items;

	cout << "string_items: ";
	copy(string_items.begin(), string_items.end(),
			ostream_iterator<string>(cout, " "));
	cout << endl;


    // 4 - napisać klasę


    MapAny m;

    m.insert("id", 1);
    m.insert("name", string("Adam"));
    m.insert("age", 33);

    try
    {
        string id = m.get<string>("id");
        cout << "id = " << id << endl;
        string name = m.get<string>("name");
        cout << "name = " << name << endl;
        string address = m.get<string>("address");
    }
    catch(invalid_value_type& e)
    {
        cout << e.what() << endl;
    }
    catch(key_not_found& e)
    {
        cout << e.what() << endl;
    }
}
