#include <iostream>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits.hpp>
#include <boost/type_traits/is_unsigned.hpp>
#include <boost/type_traits/is_integral.hpp>
#include <list>
#include <cstring>
#include <boost/assign.hpp>
#include <boost/foreach.hpp>

using namespace std;
using namespace boost::assign;


class MyType
{
public:
    typedef double nested_type;
};

void some_func(int x)
{
    cout << "void some_func(int)\n";
}


// custom type trait
template <typename T>
struct is_my_type : public boost::false_type
{
};

template <>
struct is_my_type<MyType> : public boost::true_type
{
};


// 1 - sposób
//template <typename T>
//void some_func(T t, typename T::nested_type* ptr = NULL)
//{
//    typename T::nested_type Type;

//    cout << "template<T> some_func(T)\n";
//}

// 2 sposob
//template <typename T>
//typename T::nested_type* some_func(T t)
//{
//    typename T::nested_type Type;

//    cout << "template<T> some_func(T)\n";

//    return NULL;
//}

template <typename T>
void some_func(T t,
               typename boost::disable_if<boost::is_integral<T> >::type* ptr = NULL,
               typename boost::disable_if<is_my_type<T> >::type* ptr2 = NULL)
{
    cout << "template<T> some_func(T)\n";
}

template <typename T>
void some_func(T t, typename boost::enable_if<boost::is_unsigned<T> >::type* ptr = NULL)
{
    cout << "template<unsigned T> some_func(unsigned T)\n";
}

template <typename T>
void some_func(T t, typename boost::enable_if<is_my_type<T> >::type* ptr = NULL)
{
    cout << "template<MyType> some_func(MyType)\n";
}

// ALGORITHM OPTIMIZATION

template <typename InIt, typename OutIt>
void mycopy(InIt start, InIt end, OutIt out)
{
    cout << "Generic mycopy\n";

    for(InIt it = start; it != end; ++it)
        *out++ = *it;
}

// optimized mycopy for arrays and
template <typename T>
void mycopy(T* start, T* end, T* out, typename boost::enable_if<boost::is_pod<T> >::type* ptr = NULL)
{
    cout << "Optimized copy for arrays of POD objects\n";

    memcpy(out, start, (end - start) * sizeof(T));
}

// enable_if with class templates
template <typename T, typename Enable = void>
class DataProcessor
{
public:
    void process_data()
    {
        cout << "Generic data processing" << endl;
    }
};

template <typename T>
class DataProcessor<T, typename boost::enable_if<boost::is_integral<T> >::type>
{
public:
    void process_data()
    {
        cout << "Integral data processing" << endl;
    }
};

int main()
{
    some_func(5);

    int x = 10;
    some_func(x);

    short sx = 10;
    some_func(sx);

    MyType mt;
    some_func(mt);

    some_func("test SFINAE");

    unsigned int ux = 8989;
    some_func(ux);

    cout << "\n--------------------------\n";

    list<int> lst = list_of(1)(2)(3)(4);
    list<int> dest;

    mycopy(lst.begin(), lst.end(), back_inserter(dest));

    BOOST_FOREACH(int item, dest)
    {
        cout << item << " ";
    }
    cout << "\n\n";

    int tab1[] = { 1, 2, 3, 4 };
    int tab2[4];

    mycopy(tab1, tab1+4, tab2);

    BOOST_FOREACH(int item, tab2)
    {
        cout << item << " ";
    }
    cout << "\n\n";

    string words1[] = { "str1", "str2", "str3", "str4" };
    string words2[4];

    mycopy(words1, words1+4, words2);

    BOOST_FOREACH(string item, words2)
    {
        cout << item << " ";
    }
    cout << "\n\n";

    vector<int> vec1 = list_of(1)(2)(3)(4);
    vector<int> vec2(4);

    mycopy(vec1.begin(), vec1.end(), vec2.begin());

    BOOST_FOREACH(int item, vec2)
    {
        cout << item << " ";
    }
    cout << "\n\n";

    DataProcessor<string> dps;
    dps.process_data();

    DataProcessor<int> intps;
    intps.process_data();
}

