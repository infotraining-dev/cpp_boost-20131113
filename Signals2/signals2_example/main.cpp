#include <iostream>
#include <vector>
#include <boost/signals2.hpp>

using namespace std;

void my_slot()
{
    cout << "my_slot" << endl;
}

class MySlot
{
    int id_;
public:
    MySlot(int id = 0) : id_(id)
    {
    }

    void operator()()
    {
        cout << "MySlot(" << id_ << ")"
             << endl;
    }
};

int slot_1(int x) { return x * x; }
int slot_2(int x) { return x / 2; }


template <class T>
class Combiner
{
public:
    typedef T result_type;

    template  <typename InIter>
    T operator()(InIter first, InIter last) const
    {
        for_each(first, last, [](T result) { std::cout << result << endl; });
        std::cout << "\n";

        return *first;
    }
};


class Source
{
public:
    boost::signals2::signal<void(const string&)> value_changed_;
private:
    string value_;
public:
    void set(const string& s)
    {
        value_ = s;
        value_changed_(s);
    }

    void connect(function<void(string)> slot)
    {
        value_changed_.connect(slot);
    }
};

class Observer1
{
public:
    void callback(const string& value)
    {
        cout << "Observer1 " << value << endl;
    }
};

class Observer2
{
public:
    void on_notice(const string& value)
    {
        cout << "Observer2 " << value << endl;
    }
};

int main()
{
    using namespace boost::signals2;

    signal<void()> sig;

    sig.connect(&my_slot);
    sig.connect(0, &my_slot);
    sig.connect(MySlot(1));
    sig.connect(2, MySlot(4), at_back);
    sig.connect(2, MySlot(6), at_front);
    connection conn1 = sig.connect(1, MySlot(5));
    sig.connect(0, MySlot(3));

    {
        scoped_connection conn2(sig.connect([]() { cout << "scoped_slot" << endl; }));

        sig();
    }

    cout << "\n-----------------\n\n";

    conn1.disconnect();

    sig();

    cout << "\n-----------------\n\n";

    signal<int(int), Combiner<int> > sig2;

    sig2.connect(&slot_1);
    sig2.connect(&slot_2);

    cout << "Result: " << sig2(6) << endl;

    cout << "\n-----------------\n\n";

    Source src;

    shared_ptr<Observer1> o1 = make_shared<Observer1>();

    {
        shared_ptr<Observer2> o2 = make_shared<Observer2>();

        src.value_changed_.connect(signal::slot_type(&Observer1::callback, o1.get(), _1).track(o1));
        src.value_changed_.connect(signal::slot_type(&Observer2::on_notice, o2.get(), _1).track(o2));

        src.set("Test1");
    }

    src.set("Test2");
}

